package hello

import "testing"

func TestMain( t *testing.T ) {
    //test for empty argument
    emptyResult := hello("") 
    
    if emptyResult != "Hello Test!"{
        t.Errorf("hello(\"\") failed, expected %v, got %v", "Hello Test!", emptyResult )
    }
    
    // test for valid argument
    emptyResult = hello("Mike") 
    
    if emptyResult != "Hello Mike!"{
        t.Errorf("hello(\"\") failed, expected %v, got %v", "Hello Test!", emptyResult )
    }
    
}