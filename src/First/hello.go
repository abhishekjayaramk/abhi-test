package hello

import "fmt"

// return hello greeting
func hello(user string) string {
	if len(user) == 0 {
		return "Hello Test!"
	} else {
		return fmt.Sprintf("Hello %v!", user)
	}
}

//  test function
func test() {
	fmt.Println("Hello")
}
